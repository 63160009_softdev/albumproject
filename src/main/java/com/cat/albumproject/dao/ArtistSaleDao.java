/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.albumproject.dao;

import com.cat.albumproject.helper.DatabaseHelper;
import com.cat.albumproject.model.ArtistReportSale;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Black Dragon
 */
public class ArtistSaleDao {

    public List<ArtistReportSale> getAllArtistSaleReport() {
        ArrayList<ArtistReportSale> list = new ArrayList();
        String sql = "SELECT  artists.ArtistId,  artists.Name, SUM(invoice_items.Quantity * invoice_items.UnitPrice) as total_price FROM artists\n"
                + "INNER JOIN albums\n"
                + "ON artists.ArtistId = albums.ArtistId\n"
                + "INNER JOIN tracks\n"
                + "ON albums.AlbumId = tracks.AlbumId\n"
                + "INNER JOIN invoice_items\n"
                + "ON tracks.TrackId = invoice_items.TrackId\n"
                + "INNER JOIN invoices\n"
                + "ON invoice_items.InvoiceId = invoices.InvoiceId\n"
                + "GROUP BY artists.ArtistId\n"
                + "ORDER BY total_price DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ArtistReportSale product = ArtistReportSale.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ArtistReportSale> getAllArtistSaleReport(String year) {
        ArrayList<ArtistReportSale> list = new ArrayList();
        String sql = "SELECT artists.ArtistId as artistId, artists.Name as name, SUM(invoice_items.Quantity * invoice_items.UnitPrice) as total_price FROM artists\n"
                + "INNER JOIN albums\n"
                + "ON artists.ArtistId = albums.ArtistId\n"
                + "INNER JOIN tracks\n"
                + "ON albums.AlbumId = tracks.AlbumId\n"
                + "INNER JOIN invoice_items\n"
                + "ON tracks.TrackId = invoice_items.TrackId\n"
                + "INNER JOIN invoices\n"
                + "ON invoice_items.InvoiceId = invoices.InvoiceId\n"
                + "WHERE strftime(\"%Y\", InvoiceDate) = \"2013\"\n"
                + "GROUP BY artists.ArtistId\n"
                + "ORDER BY total_price DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ArtistReportSale product = ArtistReportSale.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
