/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.albumproject.service;

import com.cat.albumproject.dao.ArtistSaleDao;
import com.cat.albumproject.model.ArtistReportSale;
import java.util.List;

/**
 *
 * @author Black Dragon
 */
public class ArtistReportService {
    public List<ArtistReportSale> getAllArtistSaleReport() {
        ArtistSaleDao dao = new ArtistSaleDao();
        return dao.getAllArtistSaleReport();
    }
    
    public List<ArtistReportSale> getAllArtistSaleReport(String year) {
        ArtistSaleDao dao = new ArtistSaleDao();
        return dao.getAllArtistSaleReport(year);
    }
}
