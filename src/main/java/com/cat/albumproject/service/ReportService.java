/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.albumproject.service;

import com.cat.albumproject.dao.SaleDao;
import com.cat.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author Black Dragon
 */
public class ReportService {
    public List<ReportSale> getReportSaleByDay() {
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }
    
    public List<ReportSale> getReportSaleByMonth() {
        SaleDao dao = new SaleDao();
        return dao.getMonthReport();
    }
    
    public List<ReportSale> getReportSaleByMonth(String year) {
        SaleDao dao = new SaleDao();
        return dao.getMonthReport(year);
    }
}
