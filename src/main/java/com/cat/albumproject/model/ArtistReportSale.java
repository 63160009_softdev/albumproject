/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.albumproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Black Dragon
 */
public class ArtistReportSale {
    int artistId;
    String name;
    double totalPrice;

    public ArtistReportSale(int artistId, String name, double totalPrice) {
        this.artistId = artistId;
        this.name = name;
        this.totalPrice = totalPrice;
    }

    public ArtistReportSale() {
    }

    public int getArtistId() {
        return artistId;
    }

    public void setArtistId(int artistId) {
        this.artistId = artistId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "ArtistReportSale{" + "artistId=" + artistId + ", name=" + name + ", totalPrice=" + totalPrice + '}';
    }
    
    public static ArtistReportSale fromRS(ResultSet rs) {
        ArtistReportSale obj = new ArtistReportSale();
        try {
            obj.setArtistId(rs.getInt("artistId"));
            obj.setName(rs.getString("name"));
            obj.setTotalPrice(rs.getDouble("total_price"));
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(ReportSale.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
}
